package com.example.videogames;

import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyVideoGamesAdapter extends RecyclerView.Adapter<MyVideoGamesAdapter.MyVideoGamesViewHolder> {

    private List<JeuVideo> mesJeux;
    public MyVideoGamesAdapter(List<JeuVideo> mesJeux) {
        this.mesJeux = mesJeux;
    }
    @NonNull
    @Override
    public MyVideoGamesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jeu_video,
                parent, false);
        return new MyVideoGamesViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull MyVideoGamesViewHolder holder, int position) {
        holder.display(mesJeux.get(position));
    }
    @Override
    public int getItemCount() {
        return mesJeux.size();
    }



    public class MyVideoGamesViewHolder extends RecyclerView.ViewHolder{
        private TextView mNameTV;
        private TextView mPriceTV;
        private ImageView mImageTV ;
        public MyVideoGamesViewHolder(@NonNull View itemView) {
            super(itemView);
            mNameTV = itemView.findViewById(R.id.name);
            mPriceTV = itemView.findViewById(R.id.price);
            mImageTV = itemView.findViewById(R.id.image) ;
        }
        void display(JeuVideo jeuVideo){
            mNameTV.setText(jeuVideo.getName());
            mPriceTV.setText(jeuVideo.getPrice() + " $");
            mImageTV.setImageResource(jeuVideo.getImage());
        }

    }
}
