package com.example.videogames;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Video Games Collection");
        int [] images ={R.drawable.supermario, R.drawable.jumanji,R.drawable.callofduty,R.drawable.leagueofwar,R.drawable.deadshooter,R.drawable.pubg} ;
        String [] names ={"Super Mario","Jumanji","Call of Duty","League of War" ,"Dead Shooter","PUBG"} ;
        double [] prices ={13.5, 24,35.8,24.5,22,46} ;

        JeuVideo Game1 = new JeuVideo(names[0],prices[0],images[0]) ;
        JeuVideo Game2 = new JeuVideo(names[1],prices[1],images[1]) ;
        JeuVideo Game3 = new JeuVideo(names[2],prices[2],images[2]) ;
        JeuVideo Game4 = new JeuVideo(names[3],prices[3],images[3]) ;
        JeuVideo Game5 = new JeuVideo(names[4],prices[4],images[4]) ;
        JeuVideo Game6 = new JeuVideo(names[5],prices[5],images[5]) ;



        ArrayList<JeuVideo> mesJeux = new ArrayList<>() ;
        mesJeux.add(Game1) ;mesJeux.add(Game2) ;mesJeux.add(Game3) ;mesJeux.add(Game4) ;mesJeux.add(Game5) ;mesJeux.add(Game6) ;

        RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
        //myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myRecyclerView.setAdapter( new MyVideoGamesAdapter(mesJeux));

        myRecyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));
    }
}
