package com.example.projet_movie_database;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    String API_KEY = "0e0ee047804f476858dd29f92dab77ba" ;
    // Index from which pagination should start
    private final int PAGE_START = 1;

    // Indicates if footer ProgressBar is shown (i.e. next page is loading)
    private boolean isLoading = false;

    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;

    // total no. of pages to load. Initial load is page 0, after which 2 more pages will load.
    private int TOTAL_PAGES = 0;

    // indicates the current page which Pagination is fetching.
    private  int currentPage = PAGE_START;

    //recycleViewer
    public RecyclerView myRecyclerView;
    //buttons
    public Button previous_page;
    public Button next_page;
    //text
    public TextView pageText;
    //builder
    public Retrofit.Builder builder;
    //Client
    public imdbClient client;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Movies Collection");
        pageText=(TextView) findViewById(R.id.page);
        previous_page=(Button) findViewById(R.id.previous);
        next_page=(Button) findViewById(R.id.next);
        previous_page.setEnabled(false);






        //Retrofit
        builder = new Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/")
                .addConverterFactory(GsonConverterFactory.create());
        try {
            Retrofit retrofit = builder.build();
            client = retrofit.create(imdbClient.class);
            Call<FilmList> call = client.getMovies(API_KEY, currentPage);

            call.enqueue(new Callback<FilmList>() {
                @Override
                public void onResponse(Call<FilmList> call, Response<FilmList> response) {
                    Log.i("Page", "this is page:" + currentPage);
                    List<Film> myMovies = response.body().getResults();

                    int total_pages = response.body().getTotalpages();
                    TOTAL_PAGES = total_pages;
                    pageText.setText(currentPage+"/"+TOTAL_PAGES);
                    myRecyclerView = findViewById(R.id.myRecyclerView);
                    myRecyclerView.setAdapter(new FilmAdapter(getApplicationContext(), myMovies));
                    myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

                }

                @Override
                public void onFailure(Call<FilmList> call, Throwable t) {
                    Log.e("Error", "Call error:" + t.toString());
                    Toast.makeText(MainActivity.this, "Error Retrieving Data!!!", Toast.LENGTH_SHORT).show();

                }
            });
        }
        catch ( Exception e){
            Log.d("Error", e.getMessage());
            Toast.makeText(this,e.toString(),Toast.LENGTH_SHORT).show();
        }
    }

    public void previousPage(View view){
        if(currentPage<=TOTAL_PAGES && currentPage>PAGE_START) {
            Log.i("PREVIOUS","<<<Page");
            currentPage--;
            if(currentPage==PAGE_START){
                previous_page.setEnabled(false);
            }
            if(currentPage>PAGE_START){
                next_page.setEnabled(true);
            }
            Call<FilmList> call = client.getMovies(API_KEY, currentPage);

            call.enqueue(new Callback<FilmList>() {
                @Override
                public void onResponse(Call<FilmList> call, Response<FilmList> response) {
                    Log.i("Page", "this is page:" + currentPage);
                    List<Film> myMovies = response.body().getResults();

                    int total_pages = response.body().getTotalpages();
                    TOTAL_PAGES = total_pages;

                    pageText.setText(currentPage+"/"+TOTAL_PAGES);
                    myRecyclerView = findViewById(R.id.myRecyclerView);
                    myRecyclerView.setAdapter(new FilmAdapter(getApplicationContext(), myMovies));
                    myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

                }

                @Override
                public void onFailure(Call<FilmList> call, Throwable t) {
                    Log.e("Error", "Call error:" + t.toString());
                    Toast.makeText(MainActivity.this, "Error Retrieving Data!!!", Toast.LENGTH_SHORT).show();

                }
            });
        }
    }


    public void nextPage(View view) {
        if(currentPage<TOTAL_PAGES && currentPage>=PAGE_START) {
            Log.i("NEXT",">>>page");
            currentPage++;
            if(currentPage==TOTAL_PAGES){
                next_page.setEnabled(false);
            }
            if(currentPage>PAGE_START){
                previous_page.setEnabled(true);
            }
            Call<FilmList> call = client.getMovies(API_KEY, currentPage);

            call.enqueue(new Callback<FilmList>() {
                @Override
                public void onResponse(Call<FilmList> call, Response<FilmList> response) {
                    Log.i("Page", "this is page:" + currentPage);
                    List<Film> myMovies = response.body().getResults();

                    int total_pages = response.body().getTotalpages();
                    TOTAL_PAGES = total_pages;
                    pageText.setText(currentPage+"/"+TOTAL_PAGES);
                    myRecyclerView = findViewById(R.id.myRecyclerView);
                    myRecyclerView.setAdapter(new FilmAdapter(getApplicationContext(), myMovies));
                    myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

                }

                @Override
                public void onFailure(Call<FilmList> call, Throwable t) {
                    Log.e("Error", "Call error:" + t.toString());
                    Toast.makeText(MainActivity.this, "Error Retrieving Data!!!", Toast.LENGTH_SHORT).show();

                }
            });
        }
    }

    public void showAbout(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Application développer par:\nOussama Benkabbou & Abir Boulif")
                .setCancelable(false)
                .setPositiveButton("Merci", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog message=builder.create();
        message.setTitle("About our App");
        message.show();

    }
}
