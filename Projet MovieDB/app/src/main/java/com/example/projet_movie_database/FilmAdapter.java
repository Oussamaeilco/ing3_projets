package com.example.projet_movie_database;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class FilmAdapter extends RecyclerView.Adapter<FilmAdapter.FilmViewHolder> {
    private List<Film> films;
    private Context context ;
    public FilmAdapter(Context context, List<Film> films){
        this.films=films;
        this.context=context;
    }

    @NonNull
    @Override
    public FilmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie,parent,false);
        return new FilmViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FilmViewHolder holder, int position) {
        holder.display(films.get(position));
    }

    @Override
    public int getItemCount() {
        return films.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class FilmViewHolder extends RecyclerView.ViewHolder{
        private ImageView image;
        private TextView name;
        private TextView date;

        public FilmViewHolder(@NonNull View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.image);
            name=itemView.findViewById(R.id.name);
            date=itemView.findViewById(R.id.date);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos=getAdapterPosition();
                    if(pos != RecyclerView.NO_POSITION){
                        Film ClickDataItem = films.get(pos) ;
                        Intent intent = new Intent(context, DetailsActivity.class) ;
                        intent.putExtra("title" , films.get(pos).getTitle()) ;
                        intent.putExtra("poster" , films.get(pos).getPosterPath()) ;
                        intent.putExtra("overview" , films.get(pos).getOverview()) ;
                        intent.putExtra("date" , films.get(pos).getReleaseDate()) ;
                        intent.putExtra("rating" , Double.toString(films.get(pos).getVoteAverage())) ;
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) ;
                        context.startActivity(intent);
                        Toast.makeText(v.getContext(),ClickDataItem.getTitle(), Toast.LENGTH_SHORT).show();

                    }
                }
            });
        }

        void display(Film film){
            name.setText(film.getTitle());
            date.setText(film.getReleaseDate());
            Picasso.get().load(film.getPosterPath()).into(image);
            //mPosterMovie.setImageResource(Integer.parseInt(movies.getPosterPath()));

        }


    }
}
