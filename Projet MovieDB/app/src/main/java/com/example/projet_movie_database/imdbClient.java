package com.example.projet_movie_database;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface imdbClient {

    /*@GET("movie/popular")
    Call<MovieResponse> getMovies(
            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") int page

    ) ;*/

    @GET("movie/popular")
    //Call<List<Film>> getMovies(@Query("api_key") String api_key);
    Call<FilmList> getMovies(@Query("api_key") String api_key,@Query("page") int page);


    // https://api.themoviedb.org/3/movie/popular?api_key=c24aa415bf5396a9632b40c2f4636331&language=en-US&page=1

}
