package com.example.projet_movie_database;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppCompatActivity {

    TextView name,rating,date,overview ;
    ImageView poster ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_details);

        name= findViewById(R.id.movieTitle) ;
        rating = findViewById(R.id.movieRating) ;
        date = findViewById(R.id.movieDate) ;
        overview = findViewById(R.id.movieSynopsis) ;
        poster = findViewById(R.id.moviePoster) ;

        Intent intentStart = getIntent() ;
        if (intentStart.hasExtra("title")){
            String movieTitle = getIntent().getExtras().getString("title") ;
            String movieRating = getIntent().getExtras().getString("rating") ;
            String movieDate = getIntent().getExtras().getString("date") ;
            String movieOverview = getIntent().getExtras().getString("overview") ;
            String moviePoster = getIntent().getExtras().getString("poster") ;


            //Picasso.with(this).load(moviePoster)
            Picasso.get().load(moviePoster).into(poster);
            name.setText(movieTitle);
            rating.setText(movieRating+"/10");
            date.setText(movieDate);
            overview.setText(movieOverview);
        }
        else {
            Toast.makeText(this,"No Data Available" , Toast.LENGTH_SHORT).show();
        }

    }
}
