package com.example.myapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RepoListAdapter extends RecyclerView.Adapter<RepoListAdapter.RepoListViewHolder> {
    private List<RepoList> mesRepos;

    public RepoListAdapter(List<RepoList> mesRepos) {
        this.mesRepos = mesRepos;
    }


    @NonNull
    @Override
    public RepoListAdapter.RepoListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repo,
                parent, false);
        return new RepoListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoListAdapter.RepoListViewHolder holder, int position) {
        holder.display(mesRepos.get(position));

    }

    @Override
    public int getItemCount() {
        return mesRepos.size();
    }

    public class RepoListViewHolder extends RecyclerView.ViewHolder {
        private TextView mNameRepo ;
        public RepoListViewHolder(@NonNull View itemView) {
            super(itemView);
            mNameRepo = itemView.findViewById(R.id.name) ;
        }

        void display(RepoList repos){

            mNameRepo.setText(repos.getRepoName());


        }
    }
}
