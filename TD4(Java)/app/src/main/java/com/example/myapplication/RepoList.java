package com.example.myapplication;

public class RepoList {

    private String name;

    public RepoList(String name) {
        this.name = name;
    }

    public String getRepoName() {
        return name;
    }
}
