package com.example.td2_android

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView

class DetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        var txt : TextView = findViewById<TextView>(R.id.user)
        txt.text=(applicationContext as NewsListApplication).login
    }

    fun itsOK(view: View) {
        val intent = Intent(this, NewsActivity::class.java)
        startActivity(intent)
        finish()
    }


    override fun onDestroy() {
        super.onDestroy()
        Log.i("Details", "terminaison de l'activité $localClassName")
    }
}
