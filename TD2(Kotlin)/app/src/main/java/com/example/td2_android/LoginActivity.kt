package com.example.td2_android

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login);

    }

    fun signIn(v: View){
        val intent = Intent(this, NewsActivity::class.java)
        var lgin :EditText=findViewById<EditText>(R.id.login)
        (applicationContext as NewsListApplication).login=lgin.text.toString();
        startActivity(intent)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("Login", "terminaison de l'activité $localClassName");
    }

}
