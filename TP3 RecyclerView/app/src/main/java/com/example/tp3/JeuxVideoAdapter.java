package com.example.tp3;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class JeuxVideoAdapter extends RecyclerView.Adapter<JeuxVideoViewHolder> {
    private List<JeuVideo> mesJeux;

    public JeuxVideoAdapter(List<JeuVideo> mesJeux){
        this.mesJeux=mesJeux;
    }
    @NonNull
    @Override
    public JeuxVideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jeu_video,parent,false);
        return new JeuxVideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JeuxVideoViewHolder holder, int position) {
        holder.display(mesJeux.get(position));
    }

    @Override
    public int getItemCount() {
        return mesJeux.size();
    }
}
