package com.example.td1_and;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    private EditText num1;
    private  EditText num2;
    private TextView res;

    public void computeAdd(View v){
        num1=(EditText) findViewById(R.id.num1);
        num2=(EditText) findViewById(R.id.num2);
        res=(TextView) findViewById(R.id.result);
        String s1=num1.getText().toString();
        String s2=num2.getText().toString();
        if(s1.equals("") ){
            s1="0";
        }
        if(s2.equals("")){
            s2="0";
        }

        int n1=Integer.parseInt(s1);
        int n2=Integer.parseInt(s2);

        int result=n1+n2;
        res.setText(""+result);
    }

}
