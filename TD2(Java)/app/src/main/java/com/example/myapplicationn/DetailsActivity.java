package com.example.myapplicationn;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "DetailsActivity";






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        setTitle("Details Activity");

        // récupérer le contexte d'application et la donnée qu'elle contient
        NewsListApplication app = (NewsListApplication) getApplicationContext();

        Button ok= findViewById(R.id.ok) ;
        ok.setOnClickListener(this);

        TextView viewlogin = findViewById(R.id.ViewLogin) ;
        viewlogin.setText("Login Identifier : " + app.getLogin());
    }

    @Override
    public void onClick(View view) {

        Intent intent;
        switch (view.getId()) {
            case R.id.ok:
                intent = new Intent(this, NewsActivity.class);
                startActivity(intent);
                Log.i(TAG, "Transition à l'activité NewsActivity");
                break;
        }
    }

    //lorqu'on appuie sur le bouton back, on sera redirigé vers l'activité precedante
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, NewsActivity.class);
        startActivity(intent);

    }



    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i(TAG, "Terminaison de l'activité "+getLocalClassName());
    }


}
