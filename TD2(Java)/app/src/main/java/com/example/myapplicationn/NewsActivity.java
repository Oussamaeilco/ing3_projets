package com.example.myapplicationn;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NewsActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "NewsActivity";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        setTitle("News Activity");
        Log.i(TAG, "Lancement de l'activité "+getLocalClassName());

        NewsListApplication app = (NewsListApplication) getApplicationContext();
        String log;




        Button details= findViewById(R.id.details) ;
        details.setOnClickListener(this);

        Button logout= findViewById(R.id.logout) ;
        logout.setOnClickListener(this);

        Button about = findViewById(R.id.about) ;
        about.setOnClickListener(this);

        TextView showlogin = findViewById(R.id.ShowLogin) ;

        Intent intent = getIntent();
        String login = intent.getStringExtra("login") ;


        showlogin.setText("Login Identifier : "+login);
        app.setLogin(login);


    }


    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.details:
                intent = new Intent(this, DetailsActivity.class);
                startActivity(intent);
                Log.i(TAG, "Transition à l'activité DetailsActivity");
                break;
            case R.id.logout:
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                Log.i(TAG, "Transition à l'activité LoginActivity");
                break;
            case R.id.about:
                String url = "http://android.busin.fr/";
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                Log.i(TAG, "Ouverture du site web");
                break ;


        }

    }



    // lorqu'on appuie sur le bouton back, on sera redirigé vers l'écran d'accueil et non l'activité précedante
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }


    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i(TAG, "Terminaison de l'activité "+getLocalClassName());
    }






}
