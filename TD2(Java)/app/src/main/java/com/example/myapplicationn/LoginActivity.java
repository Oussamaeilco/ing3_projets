package com.example.myapplicationn;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "LoginActivity";

    

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        setTitle("Login Activity");
        Log.i(TAG, "Lancement de l'activité "+getLocalClassName());

        // pr l'attribution d'écouteurs aux boutons, on définit l'activité elle meme en tt qu'écouteur
        Button login= findViewById(R.id.login) ;
        login.setOnClickListener(this);


    }


    @Override
    public void onClick(View view) {
        EditText id= findViewById(R.id.identifier) ;
        //String log= id.getText().toString() ;
        //Log.i(TAG,log) ;
        Intent intent;
        switch (view.getId()) {
            case R.id.login:
                intent = new Intent(this, NewsActivity.class);



                intent.putExtra("login",id.getText().toString());
                startActivity(intent);
                Log.i(TAG, "Transition à l'activité NewsActivity");
                break;
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }



    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i(TAG, "Terminaison de l'activité "+getLocalClassName());
    }






}
