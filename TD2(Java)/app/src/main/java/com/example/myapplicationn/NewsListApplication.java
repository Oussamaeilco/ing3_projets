package com.example.myapplicationn;




// cette classe constitue le contexte de notre application
public class NewsListApplication extends  android.app.Application{
    private String login ;


    @Override
    public void onCreate()
    {
        super.onCreate();
        this.login = null;


    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
